﻿


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;
using System.Drawing.Imaging;
using System.IO;
using System.Diagnostics;
using Emgu.CV;
using Emgu.CV.Util;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.UI;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
//using Microsoft.Bcl.Async;

namespace Glass_Slide_Debris_Deteciton
{
    public partial class Form1 : Form
    {
        OpenFileDialog Openfile = new OpenFileDialog();
        string resultfile;
        string Allresultfile;
        private Size oldSize;
        bool DebugInfo = false;
        DirectoryInfo d;
        List<double> viasizes = new List<double>();
        List<PointF> vialocations = new List<PointF>();
        int imgct = 0;
        int passcount = 0;
        int failcount = 0;
        public Form1()
        {
            InitializeComponent();


    }



    private void Form1_Load(object sender, EventArgs e)
            {
                oldSize = base.Size;

            }

        async private void button1_Click(object sender, EventArgs e)
            {


                int crtcnt = 0;


                if (Openfile.ShowDialog() == DialogResult.OK)
                {

                    string STR = Path.GetDirectoryName(Openfile.FileName) + @"\ResultsFolder\";
                    new FileInfo(STR).Directory.Create();
                    string STR1 = STR + @"\ResultsFolder\";
                    new FileInfo(STR1).Directory.Create();
                    string directory = Path.GetDirectoryName(Openfile.FileName);
                    resultfile = Path.GetDirectoryName(Openfile.FileName) + @"\ResultsFolder\" + DateTime.Now.ToString("yyyyMMdd_HHmm") + "Results.csv";
                    Console.WriteLine(resultfile);
                    File.Create(resultfile).Close();
                    d = new DirectoryInfo(directory);
                    textBox1.Text = d.ToString();
                    GC.Collect();
                    progressBar1.Maximum = 500;

                await Task.Factory.StartNew(WorkStart);//.ContinueWith(result => WorkStop());



            }

        }
        private void button2_Click(object sender, EventArgs e)
        {
            passcount = 0;
            failcount = 0;


            int crtcnt = 0;


            if (Openfile.ShowDialog() == DialogResult.OK)
            {

                string STR = Path.GetDirectoryName(Openfile.FileName) + @"\ResultsFolder\";
                new FileInfo(STR).Directory.Create();

                string directory = Path.GetDirectoryName(Openfile.FileName);
                resultfile = Path.GetDirectoryName(Openfile.FileName) + @"\ResultsFolder\" + DateTime.Now.ToString("yyyyMMdd_HHmm") + "Results.csv";
                Console.WriteLine(resultfile);
                File.Create(resultfile).Close();
                d = new DirectoryInfo(directory);
                textBox1.Text = Openfile.FileName.ToString();
                GC.Collect();
                progressBar1.Maximum = 500;
                progressBar1.Value = 400;

                Task.Factory.StartNew(WorkStart1);//.ContinueWith(result => WorkStop());



            }
        }
        async private void button3_Click(object sender, EventArgs e)
        {
            //var taskList = new List<Task>();
            using (var fbd = new FolderBrowserDialog())
            {
                fbd.SelectedPath = @"D:\Glass Slide Debris Analysis\Data\A14";
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] filesindirectory = Directory.GetDirectories(fbd.SelectedPath);
                    foreach (string subdir in filesindirectory)
                    {

                        string STR = subdir + @"\ResultsFolder\";
                        new FileInfo(STR).Directory.Create();
                        string STR1 = STR + @"\ResultsFolder\";
                        new FileInfo(STR1).Directory.Create();
                        string directory = subdir;
                        resultfile = subdir + @"\ResultsFolder\" + DateTime.Now.ToString("yyyyMMdd_HHmm") + "Results.csv";
                        Console.WriteLine(resultfile);
                        File.Create(resultfile).Close();
                        d = new DirectoryInfo(directory);
                        textBox1.Text = d.ToString();
                        GC.Collect();
                        progressBar1.Maximum = 500;

                        // Batch();
                        await Task.Factory.StartNew(WorkStart);//.ContinueWith(result => WorkStop());
                                                               //var rst = await Task.Run(() => WorkStart());
                                                               // Thread.Sleep(1000);
                                                               // Task.WaitAll(tsk);
                                                               //  var things =  await Task.WhenAll(tsks);                        // Console.WriteLine("After running MyTask. The result is " +tsk.Result); 
                                                               // await Task.WhenAll([running]);

                    }
                }
            }
        }
        async private void button4_Click(object sender, EventArgs e)
        {
            //var taskList = new List<Task>();
            using (var fbd = new FolderBrowserDialog())
            {
                fbd.SelectedPath = @"G:\ENG\Metrology\Projects\Glass Slide Debris Analysis";
                //  fbd.SelectedPath = @"C:\Users\kohuta\Documents\Geode Apps\Glass Slide Analysis";
                fbd.SelectedPath = @"D:\Glass Slide Debris Analysis\Final Data";

                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    Allresultfile = fbd.SelectedPath + @"\" + DateTime.Now.ToString("yyyyMMdd_HHmm") + "AllResults.csv";
                    File.Create(Allresultfile).Close();
                    File.AppendAllText(Allresultfile,"Run, Slide,"+ " X_loc, " + " Y_loc, " + " X_dim, " + " Y_dim, " + " Area, " + Environment.NewLine);

                    string[] filesindirectory = Directory.GetDirectories(fbd.SelectedPath);
                    foreach (string subdir1 in filesindirectory)
                    {

                        if (subdir1.Substring(subdir1.LastIndexOf('\\')+1)[0] == 'A' || subdir1.Substring(subdir1.LastIndexOf('\\') + 1)[0] == 'B')
                        {
                    

                        string[] filesindirectory2 = Directory.GetDirectories(subdir1);

                        foreach (string subdir in filesindirectory2)
                        {
                            string STR = subdir + @"\ResultsFolder\";
                            new FileInfo(STR).Directory.Create();

                            string directory = subdir;
                            resultfile = subdir + @"\ResultsFolder\" + DateTime.Now.ToString("yyyyMMdd_HHmm") + "Results.csv";
                            Console.WriteLine(resultfile);
                            File.Create(resultfile).Close();
                            d = new DirectoryInfo(directory);
                            textBox1.Text = subdir + @"\Stitched Image.bmp";
                            GC.Collect();
                            progressBar1.Maximum = 500;
                            progressBar1.Value = 400;


                            try
                            {
                                // Batch();
                                await Task.Factory.StartNew(WorkStart1);//.ContinueWith(result => WorkStop());
                                                                        //var rst = await Task.Run(() => WorkStart());
                            }
                            catch { continue; }// Thread.Sleep(1000);
                                               // Task.WaitAll(tsk);
                                               //  var things =  await Task.WhenAll(tsks);                        // Console.WriteLine("After running MyTask. The result is " +tsk.Result); 
                                               // await Task.WhenAll([running]);
                        }
                        }
                    }
                }
            }
        }


        private  void  WorkStart()
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { ProgressLabel.Text = "Stitching..."; }));
                }

                StitchImages(d);
                string finalImage = d + @"\ResultsFolder\";
                finalImage = finalImage + "Stitched Image.bmp";
                float centerx=1;
                float centery=1;
                float radius=1;
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { ProgressLabel.Text = "Finding Border..."; }));
                }
                RotatedRect corners;
               FindWindow(finalImage, out corners); 
                // DetectDebris(Openfile.FileName, centerx, centery, radius);//12,7);
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { ProgressLabel.Text = "Finding Debris..."; }));
                }
                DetectDebris(finalImage, corners);///12,7);
                                              ///if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { ProgressLabel.Text = "Process Complete"; }));
                }
           // return true;
            }

            private void WorkStart1()
            {



                string finalImage = textBox1.Text;
            
                float centerx=1;
                float centery=1;
                float radius=1;
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { ProgressLabel.Text = "Finding Border..."; }));
                }

            RotatedRect corners;
            FindWindow(finalImage, out corners); 
                // DetectDebris(Openfile.FileName, centerx, centery, radius);//12,7);
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { ProgressLabel.Text = "Finding Debris..."; }));
                }
                DetectDebris(finalImage, corners);///12,7);
                                                                   ///if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { ProgressLabel.Text = "Process Complete"; }));
                }
          //  return true;

        }


        /*   protected override void OnResize(System.EventArgs e)
           {
               base.OnResize(e);
               foreach (Control cnt in this.Controls)
               {
                   ResizeAll(cnt, base.Size);
               }
               oldSize = base.Size;
           }
           private void ResizeAll(Control cnt, Size newSize)
           {
               int iWidth = newSize.Width - oldSize.Width;
               cnt.Left += (cnt.Left * iWidth) / oldSize.Width;
               cnt.Width += (cnt.Width * iWidth) / oldSize.Width;

               int iHeight = newSize.Height - oldSize.Height;
               cnt.Top += (cnt.Top * iHeight) / oldSize.Height;
               cnt.Height += (cnt.Height * iHeight) / oldSize.Height;
           }

       */















        private void DetectDebris(string stn, RotatedRect corners)
        {
            double pixelsize = 1.818 * 1.4;
            float borderX = 6000;
            float borderY = 1500;
            float areaRatio = float.Parse(textBox2.Text);
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { Console.WriteLine("starting" + corners.Angle); }));
            }

            File.AppendAllText(resultfile, " X_loc, " + " Y_loc, " +  " X_dim, " +" Y_dim, "+ " Area, "  + Environment.NewLine);

            int pos = stn.LastIndexOf(@"\") + 1;
            string temp = stn.Substring(0, pos-1);

            pos = temp.LastIndexOf(@"\") + 1;
            string slide = temp.Substring(pos, temp.Length - pos);
            temp = temp.Substring(0, pos-1);

            pos = temp.LastIndexOf(@"\") + 1;
            string run = temp.Substring(pos, temp.Length - pos);
            Pen yellowPen = new Pen(Color.Yellow, 2);
            Pen redPen = new Pen(Color.Red, 20);
            Pen bluePen = new Pen(Color.Blue, 20);
            Pen greenPen = new Pen(Color.Green, 2);
            // float borderX = 5000;
            //float borderY = 1000;
            PointF[] verticies = corners.GetVertices();
            float[] sums = new float[4];
            for (int i = 0; i < 4; i++)
            {
                sums[i] = verticies[i].X + verticies[i].Y;
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { Console.WriteLine("sum " + sums[i]); }));
                }
            }
            float s1 = sums.Min();
            float s2 = F1(sums);
            float s3 = F(sums);
            float s4 = sums.Max();
            PointF LL = new PointF(verticies[Array.IndexOf(sums, s1)].X, verticies[Array.IndexOf(sums, s1)].Y);//corners.Center.X - corners.Size.Height / 2, corners.Center.Y - corners.Size.Width / 2);
            PointF UL = new PointF(verticies[Array.IndexOf(sums, s2)].X, verticies[Array.IndexOf(sums, s2)].Y);//corners.Center.X + corners.Size.Height / 2, corners.Center.Y - corners.Size.Width / 2);
            PointF UR = new PointF(verticies[Array.IndexOf(sums, s4)].X, verticies[Array.IndexOf(sums, s4)].Y);//corners.Center.X - corners.Size.Height / 2, corners.Center.Y + corners.Size.Width / 2);
            PointF LR = new PointF(verticies[Array.IndexOf(sums, s3)].X, verticies[Array.IndexOf(sums, s3)].Y);//corners.Center.X + corners.Size.Height / 2, corners.Center.Y + corners.Size.Width / 2);


            double angle;// = Math.Atan2((LR.Y - LL.Y), (LR.X - LL.X)) * (180 / Math.PI);

            angle = Math.Atan2((LR.Y - LL.Y), (LR.X - LL.X)) * (180 / Math.PI);
            //angle = Math.Atan2((LR.Y - LL.Y), (LR.X - LL.X)) * (180 / Math.PI);
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { Console.WriteLine("angle " + angle); }));
            }
            float height = new float[] { corners.Size.Height, corners.Size.Width }.Max();
            float width = new float[] { corners.Size.Height, corners.Size.Width }.Min();

            PointF LL_o = new PointF(LL.X + borderX, LL.Y + borderY);
            PointF LR_o = RotatePoint(new PointF(LL_o.X + height - borderX * 2, LL_o.Y), LL, angle);
            PointF UL_o = RotatePoint(new PointF(LL_o.X, LL_o.Y + width - borderY * 2), LL, angle);
            PointF UR_o = RotatePoint(new PointF(LL_o.X + height - borderX * 2, LL_o.Y + width - borderY * 2), LL, angle);

            PointF[] outterRect = new PointF[] { LL, UL, UR, LR };
            PointF[] innerRect = new PointF[] { LL_o, UL_o, UR_o, LR_o };





            Image<Gray, byte> img = new Image<Gray, byte>(stn);
            Image<Rgb, byte> img2sv = new Image<Rgb, byte>(stn);
           // CvInvoke.Polylines(img2sv, Array.ConvertAll(corners.GetVertices(), System.Drawing.Point.Round), true, new Bgr(Color.DarkOrange).MCvScalar, 200);
            //CvInvoke.Polylines(img2sv, Array.ConvertAll(corners.GetVertices(), System.Drawing.Point.Round), true, new Bgr(Color.DarkOrange).MCvScalar, 200);

            Bitmap middleman = (img2sv.ToBitmap());
            img2sv.Dispose();
            Bitmap dimg = new Bitmap(middleman);
            middleman.Dispose();

            // System.Drawing.Image dimg = img.ToBitmap();
            string STR = Path.GetDirectoryName(stn);
            //new FileInfo(STR).Directory.Create();
          //  img.Save(@"C:\Users\kohuta\Documents\Geode Apps\Glass Slide Analysis\Run 8\ResultsFolder\ResultsFolder\test1.bmp");

            Image<Gray, byte> gaus = new Image<Gray, byte>(stn);
            //.SmoothGaussian(75);
                                                                // gaus._ThresholdBinary(new Gray(Convert.ToInt64(ThreshBox.Text)), new Gray(255)); //was 25
            CvInvoke.Threshold(img, gaus,Convert.ToDouble(ThreshBox.Text), 255, ThresholdType.Binary);
            img.Dispose();
            //  gaus._ThresholdBinary(new Gray(150), new Gray(255)); //was 25
            //  var bwimg = gaus;

            //  Image<Gray, byte> cimg = bwimg;
            //  bwimg.Dispose();
            //  gaus.Dispose();

            // Image<Rgb, byte> bwimg2 = cimg.Convert<Rgb, byte>();


            //bwimg2.Dispose();
            /*Graphics filler = Graphics.FromImage(bda);


            using (var path = new GraphicsPath())
            {
                path.AddPolygon(innerRect);

                // Uncomment this to invert:
                 p.AddRectangle(this.ClientRectangle);

                using (var brush = new SolidBrush(Color.Black))
                {
                    filler.FillPath(brush, path);
                }
            }*/

            System.Drawing.Point[] outterArea1 = new System.Drawing.Point[] { new System.Drawing.Point(0, 0), System.Drawing.Point.Round(innerRect[0]), System.Drawing.Point.Round(innerRect[1]), new System.Drawing.Point(0, gaus.Height) };
            System.Drawing.Point[] outterArea2 = new System.Drawing.Point[] { new System.Drawing.Point(0, 0), System.Drawing.Point.Round(innerRect[0]), System.Drawing.Point.Round(innerRect[3]), new System.Drawing.Point(gaus.Width,0) };
            System.Drawing.Point[] outterArea3 = new System.Drawing.Point[] { new System.Drawing.Point(gaus.Width, gaus.Height), System.Drawing.Point.Round(innerRect[2]), System.Drawing.Point.Round(innerRect[1]), new System.Drawing.Point(0, gaus.Height) };
            System.Drawing.Point[] outterArea4 = new System.Drawing.Point[] { new System.Drawing.Point(gaus.Width, 0), System.Drawing.Point.Round(innerRect[3]), System.Drawing.Point.Round(innerRect[2]), new System.Drawing.Point(gaus.Width, gaus.Height) };

            VectorOfPoint outterVector = new VectorOfPoint(outterArea1);
            CvInvoke.FillPoly(gaus, outterVector, new MCvScalar(255, 255, 255)); 
             outterVector = new VectorOfPoint(outterArea2);
            CvInvoke.FillPoly(gaus, outterVector, new MCvScalar(255, 255, 255));
             outterVector = new VectorOfPoint(outterArea3);
            CvInvoke.FillPoly(gaus, outterVector, new MCvScalar(255, 255, 255));
             outterVector = new VectorOfPoint(outterArea4);
            CvInvoke.FillPoly(gaus, outterVector, new MCvScalar(255, 255, 255));

            Bitmap bda = new Bitmap((gaus.ToBitmap()));
            gaus.Dispose();

            //  bda.Save(STR+"test.bmp");

            // bda  Bitmap newBitmap = new Bitmap(bda);
            // lock image
            BitmapData bitmapData = bda.LockBits(
                new Rectangle(0, 0, bda.Width, bda.Height),
                ImageLockMode.ReadWrite, bda.PixelFormat);
            // step 2 - locating objects
            BlobCounter blobCounter = new BlobCounter();

            blobCounter.FilterBlobs = true;
            blobCounter.MinHeight = 2;// (int)(viasize / pixelsize) - (int)((viasize * LowerError) / pixelsize);// 20;//22//32
            blobCounter.MinWidth = 2;// (int)(viasize / pixelsize) - (int)((viasize * LowerError) / pixelsize);
            blobCounter.MaxHeight = 500;// (int)(viasize / pixelsize) + (int)((viasize * UpperError) / pixelsize);
            blobCounter.MaxWidth = 500;// (int)(viasize / pixelsize) + (int)((viasize * UpperError) / pixelsize);

            blobCounter.ProcessImage(bitmapData);
            Blob[] blobs = blobCounter.GetObjectsInformation();
            bda.UnlockBits(bitmapData);

            // step 3 - check objects' type and highlight
            SimpleShapeChecker shapeChecker = new SimpleShapeChecker();
            //shapeChecker.MinAcceptableDistortion = .5f;
            //shapeChecker.RelativeDistortionLimit = .12f;//.06f
          //  Graphics g = Graphics.FromImage(newBitmap);
            Graphics g1 = Graphics.FromImage(dimg);

           
            g1.DrawEllipse(yellowPen, new Rectangle((int)UR.X, (int)UR.Y, 100, 100));
            g1.DrawEllipse(redPen, new Rectangle((int)UL.X, (int)UL.Y, 100, 100));
            g1.DrawEllipse(bluePen, new Rectangle((int)LR.X, (int)LR.Y, 100, 100));
            g1.DrawEllipse(greenPen, new Rectangle((int)LL.X, (int)LL.Y, 100, 100));


            lock (bluePen)
            {
                g1.DrawPolygon(bluePen, outterRect);

            }
            lock (redPen)
            {
                g1.DrawPolygon(redPen, innerRect);
            }
            //  CvInvoke.Polylines(img2sv, Array.ConvertAll(box.GetVertices(), System.Drawing.Point.Round), true,new Bgr(Color.DarkOrange).MCvScalar, 200);
            // Console.WriteLine("WE found this many blobs:" + blobs.Length);
            PointF translated;
            for (int i = 0, n = blobs.Length; i < n; i++)
                {
                translated = RotatePoint(new PointF(blobs[i].CenterOfGravity.X, blobs[i].CenterOfGravity.Y), LL,-angle);
                translated.X -= LL.X;
                translated.Y -= LL.Y;

              //  if (this.InvokeRequired)
              //  {
              //      this.Invoke(new MethodInvoker(delegate { Console.WriteLine(translated.X+ translated.Y); }));
              //  }

                if (translated.X>borderX &&translated.Y>borderY&&translated.X<height-(borderX) && translated.Y < width - (borderY))//(blobs[i].CenterOfGravity.Y > (corners.Center.Y - corners.Size.Height / 2) && blobs[i].CenterOfGravity.Y < (corners.Center.Y + corners.Size.Height / 2) && blobs[i].CenterOfGravity.X > (corners.Center.X - corners.Size.Width / 2) && blobs[i].CenterOfGravity.X < (corners.Center.X + corners.Size.Width / 2))
                {
                    List<IntPoint> edgePoints =
                        blobCounter.GetBlobsEdgePoints(blobs[i]);
                    List<PointF> edgePoints2 = new List<PointF>();

                    passcount++;
                    // get bounding rectangle of the points list
                    IntPoint minXY, maxXY;
                    PointsCloud.GetBoundingRectangle(edgePoints, out minXY, out maxXY);
                    // get cloud's size
                    IntPoint cloudSize = maxXY - minXY;
                    // calculate center point
                    DoublePoint center2 = minXY + (DoublePoint)cloudSize / 2;
                    // calculate radius
                    float radius2 = ((float)cloudSize.X + cloudSize.Y) / 4;

                    double rectArea = Math.Min(cloudSize.X, cloudSize.Y) * Math.Min(cloudSize.X, cloudSize.Y);
                    // calculate mean distance between provided edge points
                    // and estimated circle's edge

                  


                    PointF center1;
                    float radius1;
                    AForge.Point center;
                    float radius;
                  //  bool circ = shapeChecker.IsCircle(edgePoints, out center, out radius);



                    //foreach (IntPoint point in edgePoints)
                    for (int ep = 0; ep < edgePoints.Count; ep++)
                    {
                       // dimg.SetPixel(edgePoints[ep].X, edgePoints[ep].Y, Color.Red);
                        edgePoints2.Add(new PointF((float)edgePoints[ep].X, (float)edgePoints[ep].Y));
                    }


                    Geometry.FindMinimalBoundingCircle(edgePoints2, out center1, out radius1);
                    if (blobs[i].Area / (Math.PI * radius1 * radius1) < areaRatio)
                    {
                        continue;
                    }
                    //if you want to draw a rectangle around the blob 
                    // if ((Math.Abs(blobs[i].CenterOfGravity.X - center1.X) <1) & (Math.Abs(blobs[i].CenterOfGravity.Y - center1.Y)< 1))



                    if (DebugInfo == true)
                    {
                        Console.WriteLine("the ratio is:" + blobs[i].Area / (Math.PI * radius1 * radius1));
                      //  lock (redPen)
                            //g.DrawRectangle(redPen, blobs[i].Rectangle);
                    }


                    double area = blobs[i].Area * pixelsize * pixelsize;



                        lock (yellowPen)
                            g1.DrawEllipse(yellowPen,
                           (float)(center2.X - radius2), (float)(center2.Y - radius2),
                           (float)(radius2 * 2), (float)(radius2 * 2));
                    
                    g1.DrawString(Math.Truncate(area).ToString(), new Font("Arial", 12), Brushes.White, new PointF((float)center2.X, (float)center2.Y));
                    viasizes.Add(area);

                    vialocations.Add(new PointF((float)translated.X, (float)translated.Y));

                    //Console.WriteLine("Color is :" + brightness.Intensity + "   ,   " + cnt);

                    string fn = Path.GetDirectoryName(stn) + @"\ResultsFolder\FailingImages\Img" + imgct + ".bmp";


                    double size = 2 * radius2 * pixelsize;
                    if (size == 0) { size = pixelsize; }
                   
                    try
                    {
                        File.AppendAllText(resultfile, translated.X * pixelsize + ", " + translated.Y * pixelsize + ",  " + cloudSize.X * pixelsize + ",  " + cloudSize.Y * pixelsize + ",  " + area + Environment.NewLine);
                    //    File.AppendAllText(Allresultfile, run + ", "+ slide + ", " + translated.X * pixelsize + ", " + translated.Y * pixelsize + ",  " + cloudSize.X * pixelsize + ",  " + cloudSize.Y * pixelsize + ",  " + area + Environment.NewLine);

                    }
                    catch { }


                }


                }
               //  newBitmap.Save(Path.GetDirectoryName(stn) + @"\ResultsFolder\ATestImg.bmp");


                STR = Path.GetDirectoryName(stn);
                STR = STR + @"\" + Path.GetFileNameWithoutExtension(stn) + "_DebrisDetected.bmp";
                Console.WriteLine(STR);
                dimg.Save(STR);
                Console.WriteLine("0");


                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { pictureBox1.Image = dimg; }));
                }

                Console.WriteLine("1");
                int max;
              //  Bitmap HeatMap = CreateHeatMap(vialocations, out max);
                //Console.WriteLine("created heatmap");

                STR = Path.GetDirectoryName(stn);
             //   STR = STR + @"\" + Path.GetFileNameWithoutExtension(stn) + "_HeatMapResults_Max" + max + ".bmp";
             //   HeatMap = ResizeImage(HeatMap, 600, 1200);
               // HeatMap.Save(STR);
                //pictureBox2.Image = HeatMap;
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { progressBar1.Value = 500; }));
                }

                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();

        }


        private void StitchImages(DirectoryInfo d)
            {
            double divider =  1.4;// 2.012;// 5.7;
            int Ximages = 8;
            int Yimages = 3;
                Console.WriteLine(GC.GetTotalMemory(true));
                string firstimg = d.GetFiles("*.bmp")[0].FullName;
            double imDim = 5498;
                System.Drawing.Image img = ResizeImage(System.Drawing.Image.FromFile(firstimg), Convert.ToInt32(imDim / divider), Convert.ToInt32(imDim / divider));
                Console.WriteLine(GC.GetTotalMemory(true));

                int width = img.Width-1;
                int height = img.Height-1;
                img.Dispose();
                Console.WriteLine(GC.GetTotalMemory(true));

                using (System.Drawing.Image img3 = new Bitmap(Convert.ToInt32(width * Ximages), Convert.ToInt32(height * Yimages)))//, PixelFormat.Format16bppRgb555))
                {
                    Console.WriteLine(GC.GetTotalMemory(true));

                    using (var g = Graphics.FromImage(img3))
                    {
                        //  Graphics g = Graphics.FromImage(img3);
                        int imgct = 0;
                        int totalct = 0;

                        int bgwidth = 0;
                        int bgHeight = 0;

                        g.Clear(SystemColors.AppWorkspace);


                        foreach (var file in d.GetFileSystemInfos("*.bmp").OrderBy(fs => int.Parse(Path.GetFileNameWithoutExtension(fs.Name.Substring(fs.Name.LastIndexOf("_") + 1)))))
                        {
                            // Console.WriteLine(Path.GetFileNameWithoutExtension(file.Name.Substring(file.Name.LastIndexOf("_"))));
                            System.Drawing.Image tmpimg = ResizeImage(System.Drawing.Image.FromFile(file.FullName), Convert.ToInt32(imDim / divider), Convert.ToInt32(imDim / divider));
                            lock (tmpimg)
                                g.DrawImage(tmpimg, new System.Drawing.Point(bgwidth, (height * (Yimages-1)) - bgHeight));
                            tmpimg.Dispose();
                            bgwidth = bgwidth + width;
                            imgct++;
                            totalct++;
                            if (this.InvokeRequired)
                            {
                               this.Invoke(new MethodInvoker(delegate { progressBar1.Value = totalct; }));
                            }
                            if (imgct == Ximages)
                            {
                                imgct = 0;
                                bgwidth = 0;
                                bgHeight = bgHeight + height;

                            }

                        }

                        string finalImage = d + @"\ResultsFolder\";
                        finalImage = finalImage + "Stitched Image.bmp";

                        g.Dispose();
                        lock (img3)
                   
                        img3.Save(finalImage, System.Drawing.Imaging.ImageFormat.Bmp);
                        img3.Dispose();
                    }



                }
            }



            private Bitmap CreateHeatMap(List<PointF> vialocations, out int max)
            {
                double[,] array = new double[121, 121];
                int ct = 0;
                foreach (PointF point in vialocations)
                {

                    array[Convert.ToInt32(point.X * 0.0052326), Convert.ToInt32(point.Y * 0.0052326)] = array[Convert.ToInt32(point.X * 0.0052326), Convert.ToInt32(point.Y * 0.0052326)] + viasizes[ct];
                    ct++;
                    //Console.WriteLine(array[Convert.ToInt32(point.X * 0.052326), Convert.ToInt32(point.Y * 0.052326)]);
                }
                Console.WriteLine("2");
                int maxValue = 50000;
                max = (int)array.Cast<double>().Max();
                int min = (int)array.Cast<double>().Min();
                Console.WriteLine("3");

                Bitmap HeatMap = new Bitmap(1400, 700);
                for (int i = 0; i < 1200; i++)
                {
                    for (int j = 0; j < 600; j++)
                    {
                        if (array[i / 10, j / 10] > maxValue)
                        {
                            HeatMap.SetPixel(i + 100, j + 100, IntToColor(maxValue, min, maxValue));

                        }
                        else
                        {
                            HeatMap.SetPixel(i + 100, j + 100, IntToColor((int)array[i / 10, j / 10], min, maxValue));
                        }
                    }
                }
                Console.WriteLine("4");

                Graphics ghm = Graphics.FromImage(HeatMap);


                //
                //Fill in edged black
                //
                Console.WriteLine("5");

                Rectangle ImageSize = new Rectangle(0, 0, 100, 1300);
                ghm.FillRectangle(Brushes.Black, ImageSize);
                ImageSize = new Rectangle(0, 1200 + 100, 1300, 1300);
                ghm.FillRectangle(Brushes.Black, ImageSize);
                ImageSize = new Rectangle(0, 0, 1400, 100);
                ghm.FillRectangle(Brushes.Black, ImageSize);
                ImageSize = new Rectangle(1300, 0, 1400, 1400);
                ghm.FillRectangle(Brushes.Black, ImageSize);
                Console.WriteLine("6");


                //
                //draw colorbar + min and max values
                //
                string exePath =
          System.IO.Path.GetDirectoryName(
             System.Reflection.Assembly.GetEntryAssembly().Location) + @"/ColorScale.bmp";
                Bitmap colorbar = new Bitmap(exePath);
                RectangleF rectf = new RectangleF(1145 + 200, 265 + 100, 100, 50);
                Console.WriteLine("7");

                ghm.SmoothingMode = SmoothingMode.AntiAlias;
                ghm.InterpolationMode = InterpolationMode.HighQualityBicubic;
                ghm.PixelOffsetMode = PixelOffsetMode.HighQuality;
                ghm.DrawString(maxValue.ToString(), new Font("Tahoma", 14), Brushes.White, rectf);
                rectf = new RectangleF(1162 + 200, 265 + 530 + 100, 50, 50);
                ghm.DrawString(min.ToString(), new Font("Tahoma", 14), Brushes.White, rectf);
                Console.WriteLine("8");

                colorbar = ResizeImage(colorbar, 25, 500);
                lock (colorbar)
                    ghm.DrawImage(colorbar, new System.Drawing.Point(1160 + 200, 290 + 100));
                Console.WriteLine("9");

                //
                //Draw Axis
                //
                Pen whitePen = new Pen(Color.White, 3);

                Console.WriteLine("10");

                // x axis
                lock (whitePen)
                    ghm.DrawLine(whitePen, 100, 1200 + 100, 1300, 1200 + 100);
                for (int i = 100; i < 1201; i = i + 100)
                {
                    lock (whitePen)
                        ghm.DrawLine(whitePen, 100 + i, 1290, 100 + i, 1310);
                }
                Console.WriteLine("11");

                for (int i = 100; i < 1201; i = i + 100)
                {
                    lock (whitePen)
                        ghm.DrawLine(whitePen, 90, i, 110, i);
                }
                // x axis label
                // g.DrawString("Time", new Font("Arial", 12), Brushes.White, new PointF(600, 924));
                // y axis
                Console.WriteLine("12");

                lock (whitePen)
                    ghm.DrawLine(whitePen, 100, 0 + 100, 100, 1200 + 100);
                // y axis label
                // g.DrawString("f (Hz)", new Font("Arial", 12), Brushes.White, new PointF(24, 388));
                // y axis top frequency
                // g.DrawString("20k", new Font("Arial", 12), Brushes.White, new PointF(75, 388));
                // y axis top marker
                // g.DrawLine(whitePen, 110, 388, 124, 388);
                // x axis Zero Point

                ghm.DrawString("0", new Font("Arial", 12), Brushes.White, new PointF(100, 1225 + 100));
                // y axis Zero Point
                ghm.DrawString("0", new Font("Arial", 12), Brushes.White, new PointF(75, 1200 + 100));
                ghm.DrawString("120mm", new Font("Arial", 12), Brushes.White, new PointF(1275, 1225 + 100));
                // y axis end Point
                ghm.DrawString("120mm", new Font("Arial", 12), Brushes.White, new PointF(25, 90));
                Console.WriteLine("13");

                ghm.Flush();

                ghm.Dispose();
                colorbar.Dispose();
                return HeatMap;
            }
            Color[] colors = new Color[] { Color.Black, Color.Blue, Color.Cyan, Color.Green, Color.Yellow, Color.Orange, Color.Red, Color.White, Color.Transparent };
            public Color IntToColor(int i, int min, int max)
            {
                float scaled = (float)(i - min) / (max - min) * 7;
                Color color0 = colors[(int)scaled];
                Color color1 = colors[(int)scaled + 1];
                float fraction = scaled - (int)scaled;
                Color result = new Color();
                /*
                result.R = (byte)((1 - fraction) * (float)color0.R + fraction * (float)color1.R);
                result.G = (byte)((1 - fraction) * (float)color0.G + fraction * (float)color1.G);
                result.B = (byte)((1 - fraction) * (float)color0.B + fraction * (float)color1.B);
                result.A = 255;
                 * */
                result = Color.FromArgb(255, (byte)((1 - fraction) * (float)color0.R + fraction * (float)color1.R), (byte)((1 - fraction) * (float)color0.G + fraction * (float)color1.G), (byte)((1 - fraction) * (float)color0.B + fraction * (float)color1.B));
                return result;
            }
            private Color HeatMapColor(double value, double min, double max)
            {
                Color firstColour = Color.RoyalBlue;
                Color secondColour = Color.LightSkyBlue;

                // Example: Take the RGB
                //135-206-250 // Light Sky Blue
                // 65-105-225 // Royal Blue
                // 70-101-25 // Delta

                int rOffset = Math.Max(firstColour.R, secondColour.R);
                int gOffset = Math.Max(firstColour.G, secondColour.G);
                int bOffset = Math.Max(firstColour.B, secondColour.B);

                int deltaR = Math.Abs(firstColour.R - secondColour.R);
                int deltaG = Math.Abs(firstColour.G - secondColour.G);
                int deltaB = Math.Abs(firstColour.B - secondColour.B);

                double val = (value - min) / (max - min);
                int r = rOffset - Convert.ToByte(deltaR * (1 - val));
                int g = gOffset - Convert.ToByte(deltaG * (1 - val));
                int b = bOffset - Convert.ToByte(deltaB * (1 - val));

                return Color.FromArgb(255, r, g, b);
            }




            private double CalculateStdDev(IEnumerable<double> values)
            {
                double ret = 0;
                if (values.Count() > 0)
                {
                    //Compute the Average      
                    double avg = values.Average();
                    //Perform the Sum of (value-avg)_2_2      
                    double sum = values.Sum(d => Math.Pow(d - avg, 2));
                    //Put it all together      
                    ret = Math.Sqrt((sum) / (values.Count() - 1));
                }
                return ret;
            }


            /// <summary>
            /// Resize the image to the specified width and height.
            /// </summary>
            /// <param name="image">The image to resize.</param>
            /// <param name="width">The width to resize to.</param>
            /// <param name="height">The height to resize to.</param>
            /// <returns>The resized image.</returns>
            public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
            {
                var destRect = new Rectangle(0, 0, width, height);
                var destImage = new Bitmap(width, height);

                destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                using (var graphics = Graphics.FromImage(destImage))
                {
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    using (var wrapMode = new ImageAttributes())
                    {
                        wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                        lock (image)
                            graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                    }
                    image.Dispose();
                    graphics.Dispose();
                }

                return destImage;

            }


            private Image<Gray, byte> FillHoles(Image<Gray, byte> image, int minArea, int maxArea)
            {

                Image<Gray, byte> resultImage = image.CopyBlank();
                Image<Rgb, byte> resultImage1 = image.Convert<Rgb, byte>();

                Gray gray = new Gray(255);


                VectorOfVectorOfPoint contoursDetected = new VectorOfVectorOfPoint();

                CvInvoke.FindContours(image,
                    contoursDetected, null, RetrType.List, ChainApproxMethod.ChainApproxSimple);

                List<VectorOfPoint> contoursArray = new List<VectorOfPoint>();
                int count = contoursDetected.Size;
                Console.WriteLine("detected : " + count);
                for (int i = 0; i < count; i++)
                {
                    using (VectorOfPoint currContour = contoursDetected[i])
                    {
                        contoursArray.Add(currContour);
                    }
                }

                //foreach (VectorOfPoint contours in contoursArray)
                {
                    int indexMax = contoursDetected.Size;
                    // Console.WriteLine("Contour Area: " + CvInvoke.ContourArea(contours));
                    //if ((CvInvoke.ContourArea(contours) < maxArea) && (CvInvoke.ContourArea(contours) > minArea))
                    // {
                    for (int i = 0; i < indexMax; i++)
                    {
                        Console.WriteLine("Contour Area: " + CvInvoke.ContourArea(contoursDetected[i]));
                        if ((CvInvoke.ContourArea(contoursDetected[i]) < maxArea) && (CvInvoke.ContourArea(contoursDetected[i]) > minArea))
                        {
                            CvInvoke.DrawContours(resultImage1, contoursDetected, i, new MCvScalar(0, 0, 0), -1);// Dessin des contours                }
                        }
                    }
                }
                resultImage =resultImage1.Convert<Gray, byte>();

                return resultImage;
            }

            private void FindWindow(string stn, out RotatedRect corners)
            {



            corners = new RotatedRect();

            Image<Gray, byte> img = new Image<Gray, byte>(stn);
            Image<Gray, byte> gray = new Image<Gray, byte>(new Size(img.Width, img.Height));

            Image<Gray, byte> cannyEdges = new Image<Gray, byte>(stn);
            Image<Gray, byte> binaryImg = new Image<Gray, byte>(stn);
            Image<Rgb, byte> img2sv = new Image<Rgb, byte>(stn);
                //Bitmap middleman = (img2sv.ToBitmap());
               // Bitmap dimg = new Bitmap(middleman);
           //     System.Drawing.Image drimg = img2sv.ToBitmap();
                string STR = Path.GetDirectoryName(stn) + @"\ResultsFolder\";
            // new FileInfo(STR).Directory.Create();


            {
              
                {
                    //Convert the image to grayscale and filter out the noise
                   // CvInvoke.CvtColor(img, gray, ColorConversion.Bgr2Gray);

                    //Remove noise
                    CvInvoke.GaussianBlur(img, gray, new Size(3, 3), 1);

                  //  cannyEdges.Save(STR + "gray.jpg");

                    img.Dispose();
                    #region Canny and edge detection
                    double cannyThresholdLinking = 20.0;
                    double cannyThreshold = 30.0;

                    CvInvoke.Canny(gray, cannyEdges, cannyThreshold, cannyThresholdLinking);
                  //  cannyEdges.Save(STR + "edges.jpg");

                  //  cannyEdges.Dispose();
                  //  CvInvoke.Threshold(gray, binaryImg, 50 ,255, ThresholdType.Binary);
                 //   Mat element = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Ellipse, new Size(5, 5), new System.Drawing.Point(-1, -1));


                    Image<Gray, byte> eroded = new Image<Gray, byte>(stn);


                 //   CvInvoke.Erode(cannyEdges, eroded, element, new System.Drawing.Point(-1, -1), 2 ,BorderType.Constant, new MCvScalar(0,0,0));
                    Mat element = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Ellipse, new Size(10, 10), new System.Drawing.Point(-1, -1));

                    binaryImg.Dispose();
                    Image<Gray, byte> dilated = new Image<Gray, byte>(stn);

                    CvInvoke.Dilate(cannyEdges, dilated, element, new System.Drawing.Point(-1, -1), 5, BorderType.Constant, new MCvScalar(0, 0, 0));
                //    element = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Ellipse, new Size(5, ), new System.Drawing.Point(-1, -1));

                 //   CvInvoke.Erode(eroded, dilated, element, new System.Drawing.Point(-1, -1), 3, BorderType.Constant, new MCvScalar(0, 0, 0));
                   // dilated.Save(STR + "binary.jpg");

                    eroded.Dispose();
                    //  if (this.InvokeRequired)
                    //  {
                    //      this.Invoke(new MethodInvoker(delegate { pictureBox1.Image = cannyEdges.ToBitmap(); }));
                    //  }
                    //Thread.Sleep(5000);
                    /*   LineSegment2D[] lines = CvInvoke.HoughLinesP(
                           dilated,
                           1, //Distance resolution in pixel-related units
                           Math.PI / 45.0, //Angle resolution measured in radians.
                           150, //threshold
                           20, //min Line width
                           100); //gap between lines
                    */
                    #endregion
                    float rectArea = dilated.Width*dilated.Height;
                    Console.WriteLine("Imgw" + dilated.Width);
                    Console.WriteLine("Imgh" + dilated.Height);
                    #region Find triangles and rectangles
                    List<RotatedRect> boxList = new List<RotatedRect>(); //a box is a rotated rectangle
                    using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
                    {
                        CvInvoke.FindContours(dilated, contours, null, RetrType.List,
                            ChainApproxMethod.ChainApproxSimple);
                        int count = contours.Size;
                        for (int i = 0; i < count; i++)
                        {
                            using (VectorOfPoint contour = contours[i])
                            using (VectorOfPoint approxContour = new VectorOfPoint())
                            {
                                
                                CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.05,
                                    true);
                                if (CvInvoke.ContourArea(approxContour, false) > 2500000) //only consider contours with area greater than 250
                                {
                                   
                                     if (approxContour.Size == 4) //The contour has 4 vertices.
                                    {
                                        #region determine if all the angles in the contour are within [80, 100] degree
                                        bool isRectangle = true;
                                        System.Drawing.Point[] pts = approxContour.ToArray();
                                        LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                                        for (int j = 0; j < edges.Length; j++)
                                        {
                                            double angle = Math.Abs(
                                                edges[(j + 1) % edges.Length].GetExteriorAngleDegree(edges[j]));
                                            if (angle < 80 || angle > 100)
                                            {
                                                isRectangle = false;
                                                break;
                                            }
                                        }

                                        #endregion
                                        RotatedRect tempRect;
                                        VectorOfPoint tempPt;//= new VectorOfPoint(CvInvoke.MinAreaRect(approxContour).GetVertices()[0]));
                                        if (isRectangle) {
                                            tempRect = CvInvoke.MinAreaRect(approxContour);
                                            tempPt= new VectorOfPoint(new System.Drawing.Point[] { System.Drawing.Point.Round(CvInvoke.MinAreaRect(approxContour).GetVertices()[0]), System.Drawing.Point.Round(CvInvoke.MinAreaRect(approxContour).GetVertices()[1]), System.Drawing.Point.Round(CvInvoke.MinAreaRect(approxContour).GetVertices()[2]), System.Drawing.Point.Round(CvInvoke.MinAreaRect(approxContour).GetVertices()[3]) }   );

                                            boxList.Add(tempRect);
                                            Console.WriteLine("Right" + CvInvoke.BoundingRectangle(tempPt).Right);
                                            Console.WriteLine("Left" + CvInvoke.BoundingRectangle(tempPt).Left);
                                            Console.WriteLine("Top" + CvInvoke.BoundingRectangle(tempPt).Top);
                                            Console.WriteLine("Bottom" + CvInvoke.BoundingRectangle(tempPt).Bottom);

                                            if (CvInvoke.BoundingRectangle(tempPt).Left < 0 || CvInvoke.BoundingRectangle(tempPt).Right > dilated.Width || CvInvoke.BoundingRectangle(tempPt).Bottom > dilated.Height || CvInvoke.BoundingRectangle(tempPt).Top < 0)
                                            {
                                                Console.WriteLine("rejectd");
                                                continue;
                                            }
                                            if (tempRect.Size.Width * tempRect.Size.Height < rectArea && tempRect.Size.Width * tempRect.Size.Height>200000000)
                                            {
                                                corners = tempRect;
                                                rectArea = tempRect.Size.Width * tempRect.Size.Height;
                                            }
                                           // break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

       

                   /* foreach (RotatedRect box in boxList)
                    {
                        CvInvoke.Polylines(img2sv, Array.ConvertAll(box.GetVertices(), System.Drawing.Point.Round), true,new Bgr(Color.Blue).MCvScalar, 20);
                    }*/
                    CvInvoke.Polylines(img2sv, Array.ConvertAll(corners.GetVertices(), System.Drawing.Point.Round), true, new Bgr(Color.Blue).MCvScalar, 20);



                   // img2sv.Save(STR + "result.jpg");

                }
            }

            img2sv.Dispose();
            gray.Dispose();
           // cannyEdges.Dispose();
           img.Dispose();
            gray.Dispose();

            //Image<Gray, byte> cannyEdges = new Image<Gray, byte>(stn);
            binaryImg.Dispose();
            img2sv.Dispose();
            
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();


        }






        
        /// <summary>
        /// Rotates one point around another
        /// </summary>
        /// <param name="pointToRotate">The point to rotate.</param>
        /// <param name="centerPoint">The center point of rotation.</param>
        /// <param name="angleInDegrees">The rotation angle in degrees.</param>
        /// <returns>Rotated point</returns>
        static PointF RotatePoint(PointF pointToRotate, PointF centerPoint, double angleInDegrees)
        {
            double angleInRadians = angleInDegrees * (Math.PI / 180);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);
            return new PointF
            {
                X =
                    (int)
                    (cosTheta * (pointToRotate.X - centerPoint.X) -
                    sinTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.X),
                Y =
                    (int)
                    (sinTheta * (pointToRotate.X - centerPoint.X) +
                    cosTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.Y)
            };
        }

        public static float F(float[] array)
        {
            array = array.OrderByDescending(c => c).Distinct().ToArray();
            switch (array.Count())
            {
                case 0:
                    return -1;
                case 1:
                    return array[0];
            }
            return array[1];
        }


        public static float F1(float[] array)
        {
            array = array.OrderByDescending(c => c).Distinct().ToArray();
            switch (array.Count())
            {
                case 0:
                    return -1;
                case 1:
                    return array[0];
            }
            return array[2];
        }


    }
    }


